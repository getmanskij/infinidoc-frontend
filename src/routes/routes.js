import DashboardLayout from '../layout/DashboardLayout.vue'
// GeneralViews
import NotFound from '../pages/NotFoundPage.vue'

// Admin pages
import Overview from 'src/pages/Overview.vue'
import UserProfile from 'src/pages/UserProfile.vue'
import TableList from 'src/pages/TableList.vue'
import Typography from 'src/pages/Typography.vue'
import Icons from 'src/pages/Icons.vue'
import Maps from 'src/pages/Maps.vue'
import Notifications from 'src/pages/Notifications.vue'
import Upgrade from 'src/pages/Upgrade.vue'
import ChangeDisplay from "../pages/ChangeDisplay";

const routes = [
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/greeting',
    children: [
      {
        path: 'observations',
        name: 'Overview',
        component: Overview
      },
      {
        path: 'statistics',
        name: 'User',
        component: UserProfile
      },
      {
        path: 'psychology',
        name: 'Table List',
        component: TableList
      },
      {
        path: 'changes',
        name: 'Typography',
        component: Typography
      },
      {
        path: 'changes/:id',
        component: ChangeDisplay
      },
      {
        path: 'tasks',
        name: 'Icons',
        component: Icons
      },
      {
        path: 'greeting',
        name: 'Maps',
        component: Maps
      },
      {
        path: 'notifications',
        name: 'Notifications',
        component: Notifications
      },
      {
        path: 'upgrade',
        name: 'Upgrade to PRO',
        component: Upgrade
      }
    ]
  },
  { path: '*', component: NotFound }
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
