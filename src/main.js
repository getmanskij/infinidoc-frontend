import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'

// LightBootstrap plugin
import LightBootstrap from './light-bootstrap-main'

import VueResource from 'vue-resource'

// router setup
import routes from './routes/routes'

import './registerServiceWorker'
// plugin setup
Vue.use(VueRouter);
Vue.use(LightBootstrap);
Vue.use(VueResource);

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: 'nav-item active',
  scrollBehavior: (to) => {
    if (to.hash) {
      return {selector: to.hash}
    } else {
      return { x: 0, y: 0 }
    }
  }
});


Vue.http.get(window.location.origin + '/users/authorities')
  .then(res => {
    console.log('YOYO');
    console.log(res);
    Vue.prototype.$role = res.body[0].authority.toLowerCase();

    /* eslint-disable no-new */
    new Vue({
      el: '#app',
      render: h => h(App),
      router
    });
  })
  .catch(err => {
    console.error("Failed to fetch role");
    console.error(err);
  });
